# [openSkin](https://bitbucket.org/jacole/openskin/) #

This project is to implement a radiation skin dose map from a radiation dose structured report.

It is was initiated to be used with [OpenREM](http://openrem.org) but can function as a standalone package.

It is currently under heavy development. Whilst results are plausible they have not been validated.

Being Python the code is very readable and should be fairly well commented. However, for more information including explanation of design choices see the [wiki](https://bitbucket.org/openskin/openskin/wiki/).