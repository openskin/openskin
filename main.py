""" 
    Copyright 2015 Jonathan Cole

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""

import geomclass
import numpy as np
import geomfunc
import skinMap
import png
import csv
from tkFileDialog import askopenfilename
from tkFileDialog import askdirectory
from Tkinter import *
from easygui import choicebox
from easygui import enterbox
from easygui import boolbox
import os.path

root = Tk()
root.withdraw()

myChoice = choicebox(msg='Choose a phantom type', title=' ', choices=("flat", "3D"))

patPos = choicebox(msg='Patient position', title=' ', choices=("HFS", "FFS", "HFP", "FFP", "HFLL", "FFLL", "HFRL", "FFRL"))

table = float(enterbox(msg='Enter table thickness in cm.', title=' ', default='2', strip=True))
tabWidth = float(enterbox(msg='Enter table width in cm.', title=' ', default='40', strip=True))
tabLen = float(enterbox(msg='Enter table length in cm.', title=' ', default='150', strip=True))
encode_16_bit_colour = boolbox('Grayscale or encoded 16-bit colour', ' ', ["Encoded 16-bit colour", "Greyscale"])

if myChoice == "flat":
    testPhantom = geomclass.Phantom("flat", [025, 0, 0], 50, 150, 10, 1)
    mattress = 0
elif myChoice == "3D":
    mattress = float(enterbox(msg='Enter mattress thickness in cm.', title=' ', default='4', strip=True))
    patMass = float(enterbox(msg='Enter patient mass in kg.', title=' ', default='73.2', strip=True))
    patHeight = float(enterbox(msg='Enter patient height in cm.', title=' ', default='178.6', strip=True))
    testPhantom = geomclass.Phantom_3([0, -10, -mattress], mass=patMass, height=patHeight)

tableMattress = table + mattress

csvfile = askopenfilename(title='Choose a CSV file')

saveFolder = askdirectory(title='Choose folder to save results')

f = open(csvfile, 'rU')
csv_f = csv.reader(f)

myDose = geomclass.SkinDose(testPhantom)
countRow = 0

for row in csv_f:
    refAK = float(row[11])
    myDose.addView(row)
    if not refAK == 0:
        deltax = float(row[34]) / 10.
        deltay = float(row[35]) / 10.
        deltaz = float(row[36]) / 10.
        anglex = float(row[12])
        angley = float(row[13])
        Dref = float(row[33]) / 10. - 15
        area = float(row[10]) / refAK * 100. * 100.
        kV = float(row[24])
        
        
        if patPos == "FFS":
            deltax = -deltax
            deltay = - deltay
        elif patPos == "HFP":
            deltaz = -deltaz
            deltax = -deltax 
        elif patPos == "FFP":
            deltay = -deltay
            deltaz = -deltaz
        
        
        if row[19] == "":
            filterCu = 0.0
        else:
            filterCu = float(row[19])
        countRow += 1

        typeOfRun = row[6]
        if "Rotational" not in typeOfRun:
            xRay = geomfunc.buildRay(deltax, deltay, deltaz, anglex, angley, Dref + 15)
            if testPhantom.phantomType == "3d":
                trans = geomfunc.getTableMattressTrans(kV, filterCu)
            elif testPhantom.phantomType == "flat":
                trans = geomfunc.getTableTrans(kV, filterCu)
            myDose.addDose(skinMap.skinMap(xRay, testPhantom, area, refAK, kV, filterCu, Dref, tabLen, tabWidth, trans,
                                           tableMattress, anglex, angley))
        else:
            frames = float(row[23])
            endAngle = float(row[14])
            if testPhantom.phantomType == "3d":
                trans = geomfunc.getTableMattressTrans(kV, filterCu)
            elif testPhantom.phantomType == "flat":
                trans = geomfunc.getTableTrans(kV, filterCu)
            xRay = geomfunc.buildRay(deltax, deltay, deltaz, anglex, angley, Dref + 15)
            myDose.addDose(
                skinMap.rotational(xRay, anglex, endAngle, int(frames), testPhantom, area, refAK, kV, filterCu, Dref,
                                   tabLen, tabWidth, trans, tableMattress))

count = 0
# for x in range(0,len(myDose.doseArray[0,0,:])):
# count = count + 1
# filename = os.path.join(saveFolder, 'output' + str(count).rjust(4,'0') + '.png')
# skinMap.skinMapToPng(False, myDose.doseArray[:,:,x], filename, testPhantom)

skinMap.skinMapToPng(False, myDose.totalDose, os.path.join(saveFolder, 'skin_dose_map.png'), testPhantom,encode_16_bit_colour=encode_16_bit_colour)
skinMap.writeResultsToTxt(os.path.join(saveFolder, 'skin_dose_results.txt'), csvfile, testPhantom, myDose)
